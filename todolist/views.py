from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.http import HttpResponse
from .models import ToDoItem
from .forms import LoginForm, AddTaskForm, RegisterForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone
from django.contrib.auth.hashers import make_password



def index(request):
	todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
	context = {
		"todoitem_list" : todoitem_list,
		"user": request.user
	}
	# context = {'todoitem_list' : todoitem_list}
	return render(request, "todolist/index.html", context)
	#return HttpResponse("Hello from the views.py file")


def todoitem(request, todoitem_id):
	todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
	# response = "You are viewing the details of %s"
	# return HttpResponse(response %todoitem_id)
	return render(request, "todolist/todoitem.html", model_to_dict(todoitem))


def register(request):

	context = {}

	if request.method == "POST":
		form = RegisterForm(request.POST)
		if form.is_valid() == False:
			form = RegisterForm()
		else: 
			username = form.cleaned_data['username']
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			confirm_password = form.cleaned_data['confirm_password']
			duplicate = User.objects.filter(username =  username) 

			if not duplicate:
				User.objects.create(username=username, first_name=first_name, last_name=last_name, email=email, password=make_password(password), is_staff = False, is_active = True)
				


				context = {
					"first_name" : first_name,
					"last_name" : last_name
				}
				return redirect("todolist:index")
			else: 
				context = {
                    "error" : True
                }
	return render(request, "todolist/register.html", context)

	# users = User.objects.all()
	# is_user_registered = False
	# context = {
	# 	"is_user_registered" : is_user_registered
	# }

	# # for loop
	# # check if a username "johndoe" already exist
	# # if it exist change the value of variable is_user_registered to True
	# for indiv_user in users:
	# 	if indiv_user.username == "johndoe":
	# 		is_user_registered = True
	# 		break;

	# if is_user_registered == False:
	# 	user = User() # empty object
	# 	user.username = "johndoe"
	# 	user.first_name = "John"
	# 	user.last_name = "Doe"
	# 	user.email = "john@mail.com"

	# 	#password hashing
	# 	# The set_password is used to ensure that the pw is hashed using Django's authentication framework
	# 	user.set_password("John1234")
	# 	user.is_staff = False
	# 	user.is_active = True
	# 	user.save()


	# 	context = {
	# 		"first_name" : user.first_name,
	# 		"last_name" : user.last_name
	# 	}


	# return render(request, "todolist/register.html", context)


def change_password(request):

	is_user_authenticated = False

	user = authenticate(username="johndoe", password="John1234")
	print(user)
	if user is not None:
	    authenticated_user = User.objects.get(username='johndoe') # this is where we get the user with username 'John Doe'
	    authenticated_user.set_password("johndoe1")
	    authenticated_user.save()
	    is_user_authenticated = True
	context = {
	    "is_user_authenticated": is_user_authenticated
	}

	return render(request, "todolist/change_password.html", context)


def login_view(request):

	context = {}
	if request.method == "POST":
		# create a form instance and populate it with data from the request
		form = LoginForm(request.POST)
		# Check wether the data is valid
		# runs validation routines for all the form fields and returns true and places the form's data ub tge cleaned_data attribute
		if form.is_valid() == False:
			#returns a blank login form
			form = LoginForm()
		else:
			# receives the info from the form
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = authenticate(username=username, password=password)
			context = {
				"username" : username,
				"password" : password
			}
			if user is not None: 
				login(request, user) 
				return redirect("todolist:index") 
			else:
				context = {
		            "error": True
		        }
		        # Provides context with error to conditionally render the error message
		        
                # Saves the user’s ID in the session using Django's session framework
	        
	       	

	return render(request, "todolist/login.html", context)



	# username = "johndoe"
	# password = "johndoe1"
	# user = authenticate(username=username, password=password)
	# context = {
	# 	"is_user_authenticated" : False
	# }
	# print(user)
	# if user is not None:
	# 	login(request,user)
	# 	return redirect("todolist:index")
	# else:
	# 							# templates/todolist/login.html
	# 	return render(request, "todolist/login.html", context)


def logout_view(request):
	logout(request)
	return redirect("todolist:index")



def add_task(request):
    context = {}

    if request.method == "POST":
        form = AddTaskForm(request.POST)
        if form.is_valid() == False:
            form = AddTaskForm()
        else: 
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            duplicate = ToDoItem.objects.filter(task_name =  task_name)

            if not duplicate:
                ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id = request.user.id)
                return redirect("todolist:index")

            else: 

                context = {
                    "error" : True
                }

    return render(request, "todolist/add_task.html", context)