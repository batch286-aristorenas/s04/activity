from django.urls import path

from . import views



app_name = 'todolist'
urlpatterns = [
	# the path() function receive four arguments
	# We'll focus on 2 arguments that are required, 'route(end point)' and 'view(index function)'
	# 
	path('', views.index, name='index'),

	# todolist/<todoitem_id>
	# todolist/1
	# the <int:todoitem_id>/ allows for creating a dynamic link where the todoitem_id is provided
	path('<int:todoitem_id>/', views.todoitem,name="viewtodoitem"),


	path('register', views.register, name="register"),

	path('change_password', views.change_password, name="change_password"),

	path('login', views.login_view, name="login"),

	path('logout', views.logout_view, name="logout"),

	path('add_task', views.add_task, name="add_task")
]
